#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t size = region_actual_size(size_from_capacity((block_capacity){.bytes = query}).bytes);
  void * real_addr = map_pages(addr, size, MAP_FIXED_NOREPLACE);
  if (real_addr == MAP_FAILED) {
    real_addr = map_pages(addr, size, 0);
    if (real_addr == MAP_FAILED)
        return REGION_INVALID;
  }

  block_init(real_addr, (block_size){.bytes = size}, NULL);

  return (struct region) {.addr = real_addr, .size = size, .extends = (addr == real_addr)};

}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
  struct block_header* beggining_element = (struct block_header*) HEAP_START;
  struct block_header* current_element = beggining_element;
  while (current_element) {
      beggining_element = current_element;
      size_t size = size_from_capacity(current_element->capacity).bytes;
      while (current_element && blocks_continuous(current_element, current_element->next)) {
        size += size_from_capacity(current_element->next->capacity).bytes;
        current_element = current_element->next;
      }
      if (current_element)
        current_element = current_element->next;
      munmap(beggining_element, size);
  }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static block_capacity capacity_of_block(size_t query_capacity) {
  return (block_capacity) {
    .bytes = size_max(query_capacity, BLOCK_MIN_CAPACITY)
  };
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (block == NULL) return false;
  if (!block_splittable(block, query))
    return false;
  block_size current_size_of_block = size_from_capacity(block->capacity);
  block_size first_size_of_block = size_from_capacity(capacity_of_block(query));
  block_size second_size_of_block = {
    .bytes = current_size_of_block.bytes - first_size_of_block.bytes,
  };
  void* second_pointer = (uint8_t*)block + first_size_of_block.bytes;

  block_init(second_pointer, second_size_of_block, block->next);
  block_init(block, first_size_of_block, second_pointer);
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ) {
  return  (void*) (block->contents + block->capacity.bytes);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  struct block_header* next = block->next;
  if (block == NULL || next == NULL || mergeable(block, next) == false)
    return false;
  block_init(block, (block_size) {size_from_capacity(block->capacity).bytes + size_from_capacity(next->capacity).bytes}, next->next);    
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if (block == NULL) {
    return (struct block_search_result) {
      .type = BSR_CORRUPTED,
      .block = block
    };
  }
  struct block_header *last = NULL;
  while (block) {
    while (try_merge_with_next(block));
    if (block->is_free && block_is_big_enough(sz, block))
      return (struct block_search_result) { .type = BSR_FOUND_GOOD_BLOCK, .block = block };
    last = block;
    block = block->next;      
  }
  return (struct block_search_result) { .type = BSR_REACHED_END_NOT_FOUND, .block = last};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result find_result = find_good_or_last(block, query);
    if (find_result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(find_result.block, query);
        find_result.block->is_free = false;
    }
    return find_result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (last == NULL) return NULL;

  void* addr = block_after(last);
  const struct region region = alloc_region(addr, query);
  if (region_is_invalid(&region)) {
    return NULL;
  }

  last->next = region.addr;
  if (try_merge_with_next(last) == true)
    return last;
  return last->next;
}

static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  block_capacity query_capacity = capacity_of_block(query);

  struct block_search_result result = try_memalloc_existing(query_capacity.bytes, heap_start);
  if (result.type == BSR_REACHED_END_NOT_FOUND) {  // Не нашли хороший блок
    // Если хороший блок не найден, try_memalloc_existing вернёт последний блок
    struct block_header* last_block = result.block;
    // Будем расширять кучу на вместимость блока + его заголовки (т.е. размер блока)
    block_size size_to_grow = size_from_capacity(query_capacity);
    // Расширяем кучу, получая новый блок
    struct block_header* new_last_block = grow_heap(last_block, size_to_grow.bytes);

    result = try_memalloc_existing(query_capacity.bytes, new_last_block);
  }

  return result.type == BSR_FOUND_GOOD_BLOCK ? result.block : NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  bool is_merge_possible;
  do {
    is_merge_possible = try_merge_with_next(header);
  } while (is_merge_possible);
}
