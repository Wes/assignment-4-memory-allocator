#define _DEFAULT_SOURCE

#include <assert.h>

#include "mem_internals.h"
#include "mem.h"

#define BIG_CAPACITY 5000

static void test(char* test_name, char* (*test_function)()) {
    printf(" The test has started\"%s\"...", test_name);
    heap_init(0);
    printf("%s \n", test_function());
    heap_term();
}

static struct block_header* get_last(struct block_header* heap_start) {
    struct block_header* cur_elem = heap_start;
    while (cur_elem->next) {
        cur_elem = cur_elem->next;
    }
    return cur_elem;
}


static char* min_test() {
    uint8_t* contents = _malloc(0);
    struct block_header* header = block_get_header(contents);

    assert(header->capacity.bytes == BLOCK_MIN_CAPACITY);
    assert(header->next != NULL);
    assert(!header->is_free);
    assert(header == HEAP_START);

    return "Success";
}

static char* free_test() {
    _malloc(0);
    uint8_t* malloc1 = _malloc(0);
    uint8_t* malloc2 = _malloc(0);
    uint8_t* malloc3 = _malloc(0);


    struct block_header* block1 = block_get_header(malloc1);
    struct block_header* block2 = block_get_header(malloc2);
    struct block_header* block3 = block_get_header(malloc3);

    size_t start_capacity = block2->capacity.bytes;

    _free(malloc3);
    _free(malloc2);

    size_t end_capacity = block2->capacity.bytes;

    assert(block2->is_free);
    assert(block3->is_free);
    assert(block1->next == block2);
    assert(block2->next == NULL);
    assert(end_capacity > start_capacity);

    return "Success";
}


static char* extend_old_test() {
    uint8_t* big1 = _malloc(BIG_CAPACITY);
    uint8_t* big2 = _malloc(BIG_CAPACITY);

    struct block_header* block1 = block_get_header(big1);
    struct block_header* block2 = block_get_header(big2);

    assert(block1->next == block2);
    assert((void*)(block1->contents + block1->capacity.bytes) == block2);

    return "Success";
}

static char* single_test() {
    uint8_t* contents = _malloc(BLOCK_MIN_CAPACITY + 1);
    struct block_header* header = block_get_header(contents);

    assert(header->capacity.bytes == BLOCK_MIN_CAPACITY + 1);
    assert(header->next != NULL);
    assert(!header->is_free);
    assert(header == HEAP_START);

    return "Success";
}

static char* single_free_test() {
    _malloc(0);
    uint8_t* malloc1 = _malloc(0);
    uint8_t* malloc2 = _malloc(0);


    struct block_header* block1 = block_get_header(malloc1);
    struct block_header* block2 = block_get_header(malloc2);

    _free(malloc2);

    assert(block2->is_free);
    assert(block1->next == block2);
    assert(block2->next == NULL);

    return "Success";
}

int main() {
    test("free_test", free_test);
    test("min_test", min_test);
    test("single_test", single_test);
    test("single_free_test", single_free_test);
    test("extend_old_test", extend_old_test);
}
